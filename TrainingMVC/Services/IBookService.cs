﻿using TrainingMVC.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TrainingMVC.Services
{
    public interface IBookService
    {
        Task<object> GetAll(int pageSize, int page, string accessToken);

        Task<BookDto> GetById(string id,string accessToken);

        Task<object> Add(BookDto book,string accessToken);

        Task<object> Update(string id, BookDto bookIn, string accessToken);

        Task Remove(BookDto bookIn, string accessToken);

        Task<object> Remove(string id, string accessToken);

    }
}
