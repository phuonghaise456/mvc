﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;
using TrainingMVC.Models;
using System.Linq;
using System.Threading.Tasks;
using TrainingMVC.Dtos;
using AutoMapper;
using TrainingMVC.Utils;
using System.Net.Http;
using IdentityModel.Client;
using Newtonsoft.Json;
using System.Text;

namespace TrainingMVC.Services
{
    public class BookService : IBookService
    {

        //private readonly IMongoCollection<Book> _books;
        private readonly IMapper _mapper;
        private readonly IHttpClientFactory _httpClientFactory;

        public BookService(IMapper mapper, IHttpClientFactory httpClientFactory)
        {
            //_books = dbContext.Books;
            _mapper = mapper;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<Object> GetAll(int pageSize, int page, string accessToken)
        {
            //using client Credentials
            //var serverClient = _httpClientFactory.CreateClient();
            //var discoveryDocument = await serverClient.GetDiscoveryDocumentAsync("https://localhost:7001");
            //var tokenResponse = await serverClient.RequestClientCredentialsTokenAsync(
            //    new ClientCredentialsTokenRequest
            //    {
            //        Address = discoveryDocument.TokenEndpoint,

            //        ClientId = "client_id_mvc",
            //        ClientSecret = "client_secret_mvc",
            //        Scope = "BookApi"
            //    });
            //var apiClient = _httpClientFactory.CreateClient("BookApi");
            //apiClient.SetBearerToken(tokenResponse.AccessToken);


            var apiClient = _httpClientFactory.CreateClient("BookApi");
            apiClient.SetBearerToken(accessToken);

            var response = await apiClient.GetAsync("/api/Books/GetAll?page=" + page + "&pageSize=" + pageSize);
            var content = await response.Content.ReadAsStringAsync();
            var results = JsonConvert.DeserializeObject<BookResult>(content);

            return results;
        }

        public async Task<BookDto> GetById(string id, string accessToken)
        {
            var apiClient = _httpClientFactory.CreateClient("BookApi");
            apiClient.SetBearerToken(accessToken);

            var response = await apiClient.GetAsync("/api/Books/GetById/" + id);
            var content = await response.Content.ReadAsStringAsync();
            var results = JsonConvert.DeserializeObject<Book>(content);

            return _mapper.Map<BookDto>(results);
        }

        public async Task<Object> Add(BookDto bookIn, string accessToken)
        {
            StringContent content = new StringContent(JsonConvert.SerializeObject(_mapper.Map<Book>(bookIn)), Encoding.UTF8, "application/json");

            var apiClient = _httpClientFactory.CreateClient("BookApi");
            apiClient.SetBearerToken(accessToken);

            var response = await apiClient.PostAsync("/api/Books/Add", content);
            string resultContent = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Object>(resultContent);
        }

        public async Task<Object> Update(string id, BookDto bookIn, string accessToken)
        {
            //Action<object> act = (object obj) =>
            //{
            //    dynamic arg = obj;
            //    var book = _mapper.Map<Book>((BookDto)arg.book);
            //    var id = (string)arg.ID;
            //    _books.ReplaceOne(b => b.Id == id, book);
            //};
            //Task updateData = new Task(act, new { ID = id, book = bookIn });
            //updateData.Start();
            //await updateData;

            StringContent content = new StringContent(JsonConvert.SerializeObject(_mapper.Map<Book>(bookIn)), Encoding.UTF8, "application/json");

            var apiClient = _httpClientFactory.CreateClient("BookApi");
            apiClient.SetBearerToken(accessToken);

            var response = await apiClient.PutAsync("/api/Books/Update/" + id, content);
            string resultContent = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Object>(resultContent);
        }

        public async Task Remove(BookDto bookIn, string accessToken)
        {
            //Action<object> act = (object obj) =>
            //{
            //    var book = _mapper.Map<Book>((BookDto)obj);
            //    _books.DeleteOne(b => b.Id == book.Id);
            //};
            //Task removeData = new Task(act, bookIn);
            //removeData.Start();
            //await removeData;
        }

        public async Task<Object> Remove(string id, string accessToken)
        {
            //Action<object> act = (object id) =>
            //{
            //    _books.DeleteOne(b => b.Id == (string)id);
            //};
            //Task removeData = new Task(act, id);
            //removeData.Start();
            //await removeData;

            //StringContent content = new StringContent(JsonConvert.SerializeObject(_mapper.Map<Book>(bookIn)), Encoding.UTF8, "application/json");

            var apiClient = _httpClientFactory.CreateClient("BookApi");
            apiClient.SetBearerToken(accessToken);

            var response = await apiClient.DeleteAsync("/api/Books/Delete/" + id);
            string resultContent = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Object>(resultContent);
        }
    }
}