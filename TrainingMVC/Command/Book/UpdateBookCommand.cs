using System;
using MediatR;
using TrainingMVC.Dtos;

namespace TrainingMVC.Command.Book
{
    public class UpdateBookCommand : IRequest<Object>
    {
        public BookDto BookDto { get; }
        public string Id { get; }
        public string AccessToken { get; }


        public UpdateBookCommand(BookDto bookDto, string id, string accessToken)
        {
            this.BookDto = bookDto;
            this.Id = id;
            this.AccessToken = accessToken;
        }
    }
}