using System;
using MediatR;

namespace TrainingMVC.Command.Book
{
    public class DeleteBookCommand : IRequest<Object>
    {
        public string Id { get; }
        public string AccessToken { get; }


        public DeleteBookCommand(string id, string accessToken)
        {
            Id = id;
            AccessToken = accessToken;
        }
    }
}