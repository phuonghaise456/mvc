using System;
using MediatR;
using TrainingMVC.Dtos;

namespace TrainingMVC.Command.Book
{
    public class NewBookCommand : IRequest<Object>
    {
        public BookDto BookDto { get; }
        public string AccessToken { get; }

        public NewBookCommand(BookDto bookDto, string accessToken)
        {
            BookDto = bookDto;
            AccessToken = accessToken;
        }
    }
}