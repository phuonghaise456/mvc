﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using TrainingMVC.Command.Book;
using TrainingMVC.Dtos;
using TrainingMVC.Query.Book;
using TrainingMVC.Query.Category;

namespace TrainingMVC.Controllers
{
    public class BookController : Controller
    {
        private IToastNotification _toastNotification;
        private IMediator _mediator;
        private IHttpClientFactory _httpClientFactory;

        public BookController(IMediator mediator, IToastNotification toastNotification, IHttpClientFactory httpClientFactory)
        {
            _mediator = mediator;
            _toastNotification = toastNotification;
            _httpClientFactory = httpClientFactory;
        }
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Index(int? page)
        {
            var accessToken = await GetAccessToken();

            dynamic bookDtos = await _mediator.Send(new GetAllBookQuery(page ?? 1, accessToken));

            if (((List<BookDto>)bookDtos.books).Count == 0)
            {
                bookDtos = await _mediator.Send(new GetAllBookQuery((int)--page, accessToken));

            }
            ViewBag.totalPage = (int)bookDtos.totalPages;
            ViewBag.currentPage = page ?? 1;

            //await RefreshToken();


            return View((List<BookDto>)bookDtos.books);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Insert()
        {
            var accessToken = await GetAccessToken();

            var categoryDtos = await _mediator.Send(new GetAllCategoryQuery(accessToken));
            var tuple = new Tuple<List<CategoryDto>, BookDto>(categoryDtos, new BookDto());
            return View(tuple);
        }

        [HttpPost]
        public async Task<IActionResult> Insert([Bind(Prefix = "Item2")] BookDto bookDto)
        {
            var accessToken = await GetAccessToken();

            switch (ModelState.IsValid)
            {
                case true:
                    var response = await _mediator.Send(new NewBookCommand(bookDto, accessToken));
                    _toastNotification.AddSuccessToastMessage("Added Book");
                    return RedirectToAction(nameof(Index));
                case false:
                    var categoryDtos = await _mediator.Send(new GetAllCategoryQuery(accessToken));
                    var tuple = new Tuple<List<CategoryDto>, BookDto>(categoryDtos, new BookDto());
                    return View(tuple);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Update(string id)
        {
            var accessToken = await GetAccessToken();

            var bookDto = await _mediator.Send(new GetByIdBookQuery(id, accessToken));
            if (bookDto == null)
            {
                return NotFound();
            }

            var categoryDtos = await _mediator.Send(new GetAllCategoryQuery(accessToken));
            var tuple = new Tuple<List<CategoryDto>, BookDto>(categoryDtos, bookDto);
            return View(tuple);
        }

        [HttpPost]
        public async Task<IActionResult> Update([Bind(Prefix = "Item2")] BookDto bookDto)
        {
            var accessToken = await GetAccessToken();

            switch (ModelState.IsValid)
            {
                case true:
                    var response = await _mediator.Send(new UpdateBookCommand(bookDto, bookDto.Id, accessToken));
                    _toastNotification.AddSuccessToastMessage("Updated Book");
                    return RedirectToAction("Index");
                case false:
                    var categoryDtos = await _mediator.Send(new GetAllCategoryQuery(accessToken));
                    var tuple = new Tuple<List<CategoryDto>, BookDto>(categoryDtos, bookDto);
                    return View(tuple);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int page, string id)
        {
            var accessToken = await GetAccessToken();

            var currentPage = page;
            var response = await _mediator.Send(new DeleteBookCommand(id, accessToken));
            _toastNotification.AddSuccessToastMessage("Removed Book");
            return RedirectToAction("Index", new { page = currentPage });
        }

        public async Task<string> GetIdToken()
        {
            return await HttpContext.GetTokenAsync("id_token");
        }

        public async Task<string> GetAccessToken()
        {
            return await HttpContext.GetTokenAsync("access_token");
        }

        public async Task<string> GetRefreshToken()
        {
            return await HttpContext.GetTokenAsync("refresh_token");
        }

        public async Task RefreshToken()
        {
            var serverClient = _httpClientFactory.CreateClient();
            var discoveryDocument = await serverClient.GetDiscoveryDocumentAsync("https://localhost:7001");

            var refreshToken = await GetRefreshToken();
            var refreshTokenClient = _httpClientFactory.CreateClient();

            var tokenResponse = await refreshTokenClient.RequestRefreshTokenAsync(new RefreshTokenRequest
            {
                Address = discoveryDocument.TokenEndpoint,
                ClientId = "client_id_mvc,",
                ClientSecret = "client_secret_mvc",
                GrantType = "refresh_token",
                RefreshToken = refreshToken,
            });

            var authInfo = await HttpContext.AuthenticateAsync("Cookie");

            authInfo.Properties.UpdateTokenValue("id_token", tokenResponse.IdentityToken);
            authInfo.Properties.UpdateTokenValue("access_token", tokenResponse.AccessToken);
            authInfo.Properties.UpdateTokenValue("refresh_token", tokenResponse.RefreshToken);

            await HttpContext.SignInAsync("Cookie", authInfo.Principal, authInfo.Properties);




        }



    }
}
