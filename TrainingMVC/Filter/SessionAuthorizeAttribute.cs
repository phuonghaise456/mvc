﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace TrainingMVC.Filter
{
    public class SessionAuthorizeAttribute : ActionFilterAttribute
    {
        public IHttpContextAccessor _httpContextAccessor;
        public SessionAuthorizeAttribute(IHttpContextAccessor HttpContextAccessor)
        {
            _httpContextAccessor = HttpContextAccessor;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (_httpContextAccessor.HttpContext.Session.GetString("username") == null)
            {
                filterContext.Result = new RedirectResult("~/Login/Login");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}