﻿using System.Collections.Generic;
using MediatR;
using TrainingMVC.Dtos;

namespace TrainingMVC.Query.Category
{
    public class GetAllCategoryQuery : IRequest<List<CategoryDto>>
    {
        public string AccessToken { get; }

        public GetAllCategoryQuery(string accessToken)
        {
            this.AccessToken = accessToken;
        }
    }
}
