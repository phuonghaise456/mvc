﻿using System;
using MediatR;

namespace TrainingMVC.Query.Book
{
    public class GetAllBookQuery : IRequest<Object>
    {
        public int Page { get; }

        public string AccessToken { get; }


        public GetAllBookQuery(int page, string accessToken)
        {
            this.Page = page;
            this.AccessToken = accessToken;
        }
    }
}
