﻿using MediatR;
using TrainingMVC.Dtos;

namespace TrainingMVC.Query.Book
{
    public class GetByIdBookQuery : IRequest<BookDto>
    {
        public string Id { get; }
        public string AccessToken { get; }

        public GetByIdBookQuery(string id, string accessToken)
        {
            Id = id;
            AccessToken = accessToken;
        }
    }
}
