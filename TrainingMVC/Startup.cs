using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TrainingMVC.Services;
using TrainingMVC.Utils;
using MediatR;
using System;

namespace TrainingMVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddSession();

            services.AddAuthentication(config =>
            {
                config.DefaultScheme = "Cookie";
                config.DefaultChallengeScheme = "oidc";
            })
            .AddCookie("Cookie")
            .AddOpenIdConnect("oidc", config =>
            {
                config.Authority = "https://localhost:7001";

                config.ClientId = "client_id_mvc";
                config.ClientSecret = "client_secret_mvc";
                config.ResponseType = "code";

                config.SaveTokens = true; // persist token in cookie

                config.Scope.Add("BookApi");
                config.Scope.Add("openid");
                config.Scope.Add("profile");
                config.Scope.Add("offline_access");
            });

            services.AddHttpClient("BookApi", client =>
            {
                client.BaseAddress = new Uri("https://localhost:5001");
            });

            services.AddAutoMapper(typeof(Startup));
            services.AddMvc().AddNToastNotifyToastr();
            services.AddMediatR(typeof(Startup));
            services.AddTransient<DbContext>();
            services.AddTransient<IBookService, BookService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IAuthenticationUserService, AuthenticationUserService>();

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseNToastNotify();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Book}/{action=Index}");
            });
        }
    }
}
